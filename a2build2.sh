#/bin/bash

set -e

mkdir -p $HOME/csc369/root

./configure

cd kern/conf
./config ASST2-RAND

cd ../compile/ASST2-RAND
bmake depend
bmake
bmake install

cd ../../..

if [ "$1" = "-a" ]; then
bmake
bmake install
fi

cp ./sys161.conf $HOME/csc369/root/sys161.conf

#if [ "$1" = "-r" ]; then
#cd $HOME/cscc69/root
#sys161 kernel
#fi
